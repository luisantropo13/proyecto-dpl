const bodyparser = require("body-parser");
const express = require("express");
const path = require("path");
const app = express();
const libro = require("./clases/Libro");
const MongoClient = require('mongodb').MongoClient;



let libros = [
    new libro.libro(1 , "libroa" , "No hay descripción" ),
    new libro.libro(1 , "libroa" , "No hay descripción" ),
    new libro.libro(1 , "libroa" , "No hay descripción" ),
    new libro.libro(1 , "libroa" , "No hay descripción" ),
];

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyparser.urlencoded({extended:true}));


app.get("/",function(req,res){ 
    res.render("index.pug", { libros : libros } );
});

app.get("/update" , ( req , res ) => 
{
    res.render("update.pug", { libros : libros } );
});

app.get("/delete", (req,res) => {
    res.render("delete.pug", {libros:libros});
});

app.post("/update" , ( req , res ) => 
{
    libros.push(new libro.libro(1 , req.body.nombre , req.body.descr ))
    res.render("update.pug", { libros : libros } );
});



app.listen(3000);
